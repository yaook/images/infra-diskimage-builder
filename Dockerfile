##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

FROM python:3.13.1-slim-bullseye

RUN set -eux ; \
    apt-get update ; \
    apt-get install -y --no-install-recommends \
        kpartx \
        gdisk \
        dosfstools \
        python3 \
        sudo \
        qemu-utils \
        libgcrypt20 \
        mdadm \
        cryptsetup \
        lvm2 ; \
    pip3 install --no-cache-dir diskimage-builder ironic-python-agent-builder ; \
    rm -rf /var/lib/apt/lists/*

CMD bash
