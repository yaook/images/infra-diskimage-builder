<!--
Copyright (c) 2021 The Yaook Authors.
This file is part of Yaook.
See https://yaook.cloud for further info.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# infra-diskimage-builder

Image containing diskimage-builder and the dependencies for our custom RAID-image scripts.

## Usage

To build disk images, the container needs to run privileged and have access to the hosts `/dev` directory.

    docker build . -t infra-diskimage-builder:latest
    docker run -it --rm --privileged --mount type=bind,source=/dev,destination=/dev infra-diskimage-builder:latest COMMAND

If you want a persistent diskimage-builder cache, you can mount a volume to the location set in DIB\_IMAGE\_CACHE (`/root/.cache/image-create` by default).

## License

[Apache 2](LICENSE.txt)
